package es.siani.aspem.services;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import es.siani.aspem.AspemException;

public class ServicesHelper {
	
    protected static String MSG_BAD_REQUEST =
            "Input data is not valid.";
    
    protected static String MSG_INTERNAL_SERVER_ERROR =
    		"Internal error. Ok, this is my fault.";

    protected static String MSG_UNKNOWN_ERROR =
            "Unknown error.";

	
	/**
	 * Build a response from a Status code.
	 */
	public static Response clientResponse(Response.Status status) {
		return Response.status(status).entity(getMessage(status)).build();
	}
	
	
	/**
	 * Get a string message from a status code.
	 */
	private static String getMessage(Response.Status status) {
		
		if (status == Response.Status.BAD_REQUEST) {
			return MSG_BAD_REQUEST;
		} else if (status == Response.Status.INTERNAL_SERVER_ERROR) {
			return MSG_INTERNAL_SERVER_ERROR;
		}
		
		return MSG_UNKNOWN_ERROR;
	}
	
	/**
	 * Marshall a JAXB Object to XML.
	 */
	public static<T> String ObjectToXML(T o) {
		
		String xml;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Marshaller marshaller = JAXBContext.newInstance(o.getClass()).createMarshaller();
			marshaller.marshal(o, baos);
			xml = baos.toString("UTF-8");
		} catch (Exception ex) {
			getLogger().severe("The object of the class " + o.getClass().getName() + " could not be marshalled to XML.");
			return "";
		}
		
		return xml;
	}		
	
	
	/**
	 * XML to Object.
	 * @throws GridopException 
	 */
	@SuppressWarnings("unchecked")
	public static<T> T XMLToObject(Class<T> oClass, String xml) throws AspemException {
		StringReader reader = new StringReader(xml);
		T output;
		try {
			output= (T)JAXBContext.newInstance(oClass).createUnmarshaller().unmarshal(reader);
		} catch (JAXBException e) {
			getLogger().severe("The XML string could not be converted to an object of the class " + oClass.getName());
			throw new AspemException();
		}
		
		return output;
	}
	
	
	/**
	 * Logger. 
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(ServicesHelper.class.getName());
    }		
}
