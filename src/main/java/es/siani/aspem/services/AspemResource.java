package es.siani.aspem.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/aspem")
public class AspemResource {
	
	/**
	 * Return the status of the ASPEM.
	 */
	@Path("/status")
	@GET
	@Produces("text/plain")
	public Response getStatus() {
		
		// TODO-IGN: Remove all this code.
//		AspemManager aspemManager = injector().getInstance(AspemManager.class);
//		boolean status = false;
//		try {
//			status = aspemManager.getStatus();
//		} catch (AspemException ex) {
//			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
//		}
//		
//		return Response.ok().entity(Boolean.toString(status)).build();
		
		return Response.ok().entity(Boolean.toString(true)).build();
	}
	

}
