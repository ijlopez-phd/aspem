package es.siani.aspem.services;

import static es.siani.aspem.injection.InjectionUtils.injector;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.siani.aspem.AspemException;
import es.siani.aspem.action.SimulationManager;
import es.siani.aspem.model.AsBox;

@Path("/simulation")
public class SimulationResource {
	
	/**
	 * Service for indicating that a new simulation is running.
	 */
	@POST
	@Path("{id}/start")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response start(@PathParam("id") int idSimulation) {
		
		SimulationManager simulationManager = injector().getInstance(SimulationManager.class);
		
		try {
			simulationManager.initSimulation(idSimulation);
		} catch (AspemException ex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}		
		return Response.ok().build();
	}
	
	
	/**
	 * Service for indicating that a simulation has finished.
	 */
	@PUT
	@Path("{id}/finished")
	public Response finished(
			@PathParam("id") int idSimulation) {
		
		SimulationManager simulationManager = injector().getInstance(SimulationManager.class);
		
		try {
			simulationManager.finishSimulation(idSimulation);
		} catch (AspemException ex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().build();
	}
	
	
	/**
	 * Activate the AS-Box of a client for a simulation.
	 */
	@POST
	@Path("{id}/asbox")
	@Consumes(MediaType.APPLICATION_XML)
	public Response addAgent(
			@PathParam("id") int idSimulation,
			@QueryParam("code") String clientCode,
			String xmlUserPrefs) {
		
		SimulationManager simulationManager = injector().getInstance(SimulationManager.class);
		
		try {
			simulationManager.addAsBox(new AsBox(clientCode, idSimulation), xmlUserPrefs);
		} catch (IllegalArgumentException ex) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		} catch (AspemException ex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().build();
	}
}
