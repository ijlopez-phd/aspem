package es.siani.aspem.action;

import java.util.List;
import es.siani.aspem.AspemException;
import es.siani.aspem.model.Aspem;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;

public interface GridopServicesCaller {

	/**
	 * Register or update Aspem.
	 */
	public Integer postAspem(Aspem aspem) throws AspemException;
	
	
	/**
	 * Set the status of the Aspem.
	 */
	public void putStatus(int idAspem, boolean status) throws AspemException;
	
	
	/**
	 * Delete a client.
	 */
	public void deleteClient(int idClient) throws AspemException;
	
	
	/**
	 * Register a new Client.
	 */
	public Client postClient(Client client, Aspem aspem) throws AspemException;
	
	
	/**
	 * Update a Client.
	 */
	public void putClient(Client client) throws AspemException;
	
	
	/**
	 * Get all the Clients Groups.
	 */
	public List<ClientsGroup> getAllClientsGroups() throws AspemException;
}