package es.siani.aspem.action;

import java.util.List;
import javax.inject.Inject;
import es.siani.aspem.AspemException;
import es.siani.aspem.data.ClientDataProvider;
import es.siani.aspem.data.DataProviderListener;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;

public class ClientManager {
	
	private final ClientDataProvider clientDao;
	private final GridopServicesCaller wsCaller;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public ClientManager(
			ClientDataProvider clientDao,
			GridopServicesCaller wsCaller) {
		
		this.clientDao = clientDao;
		this.wsCaller = wsCaller;
		return;
	}
	
	
	/**
	 * Save a Client item.
	 */
	public void createClient(Client client) throws AspemException {
		
		// TODO-IGN: Refact all this code.
		// Check if the ASPEM has been registered
//		Aspem aspem = this.aspemDao.getFirst();
//		if ((aspem == null) || (aspem.getGlobalId() == null)) {
//			// The ASPEM hasn't been registered with the Grid Operator
//			throw new AspemException("The ASPEM hasn't been registered with the Grid Operator");
//		}
//		
//		// Register the changes on the Grid Operator side
//		if (client.getGlobalId() == null) {
//			Client gridopClient = this.wsCaller.postClient(client, aspem);
//			client.setGlobalId(gridopClient.getId());
//			client.setXmppAddress(gridopClient.getXmppAddress());
//			client.setRemoteXmppAddress(gridopClient.getRemoteXmppAddress());
//		} else {
//			this.wsCaller.putClient(client);
//		}
//		
//		// Save the client in the ASPEM side
//		this.clientDao.save(client);
		
		return;
	}	
	
	
	/**
	 * Delete a Client item.
	 */
	public void deleteClient(Client client) throws AspemException {
		
		this.wsCaller.deleteClient(client.getGlobalId());
		this.clientDao.delete(client.getId());
		
		return;
	}	
	
	
	/**
	 * Get all Client items
	 */
	public List<Client> getAllClients() throws AspemException {
		return this.clientDao.getAll();
	}
	
	
	/**
	 * Get a Client item by using the Id property.
	 */
	public Client getClientById(int id) throws AspemException {
		return this.clientDao.getById(id);
	}
	
	
	/**
	 * Get a Client by the Code property.
	 */
	public Client getClientByCode(String code) throws AspemException {
		return this.clientDao.getByCode(code);
	}
	
	
	/**
	 * Get all Clients Groups from the Grid Operator.
	 */
	public List<ClientsGroup> getAllClientsGroups() throws AspemException {
		return this.wsCaller.getAllClientsGroups();
	}
	
	
	/**
	 * Get all Clients Groups attached to a Client.
	 */
	public List<ClientsGroup> getClientsGroups(int idClient) throws AspemException {
		return this.clientDao.getClientsGroups(idClient);
	}
	

	/**
	 * Add a listener to this data provider.
	 */
	public void addListener(DataProviderListener<Client> listener) {
		this.clientDao.addListener(listener);
		return;
	}
	
	
//	/**
//	 * Logger.
//	 */
//	private static final Logger getLogger() {
//		return Logger.getLogger(ClientManager.class.getName());
//	}
}