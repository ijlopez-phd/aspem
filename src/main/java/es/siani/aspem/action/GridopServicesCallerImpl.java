package es.siani.aspem.action;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import es.siani.aspem.AspemException;
import es.siani.aspem.config.Configuration;
import es.siani.aspem.model.Aspem;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;
import es.siani.aspem.services.ServicesHelper;


public class GridopServicesCallerImpl implements GridopServicesCaller {
	
	private static final int HTTP_OK = ClientResponse.Status.OK.getStatusCode();
	private static final int HTTP_CREATED = ClientResponse.Status.CREATED.getStatusCode();	
	
	protected final String srvEndPoint;
	protected final Configuration config;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public GridopServicesCallerImpl(Configuration config) {
		this.config = config;
		this.srvEndPoint = config.get(Configuration.PROP_GRIDOP_SERVICES_POINT);
		return;
	}
	
	
	/**
	 * @see es.siani.aspem.action.GridopServicesCaller#postAspem(es.siani.aspem.model.Aspem)
	 */
	public Integer postAspem(Aspem aspem) throws AspemException {
		
		if (aspem == null) {
			throw new IllegalArgumentException("The parameter 'aspem' must contain a valid instance of the class Aspem.");
		}
		
		// TODO-IGN: Refactor all this code.
		// Fill the params to be sent.
		Form formData = new Form();
		//formData.add("globalId", (aspem.getGlobalId() != null)? aspem.getGlobalId().toString() : null);
		formData.add("code", aspem.getCode());
		formData.add("name", aspem.getName());
		// TODOIGN: Fix this.
		//formData.add("url", config.getAspemUrl());
		
		// Make the call to the service.
		com.sun.jersey.api.client.Client client = com.sun.jersey.api.client.Client.create();		
		WebResource call = client.resource(srvEndPoint);
		Integer globalId = null;
		try {
			ClientResponse response= call.path("/aspem")
					.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
					.accept(MediaType.TEXT_PLAIN)
					.post(ClientResponse.class, formData);
			
			final int status = response.getStatus();
			if ((status == HTTP_CREATED) || (status == HTTP_OK)) {
				globalId = Integer.parseInt(response.getEntity(String.class));
			} else {
				throw new AspemException();
			}
			
		} catch (Exception ex) {
			throw new AspemException("The changes on the ASPEM entity could not be registered with the Grid Operator.");
		}
		
		return globalId;
	}
	
	
	/**
	 * @see es.siani.aspem.action.GridopServicesCaller#putStatus(int,boolean)
	 */
	public void putStatus(int idAspem, boolean status) throws AspemException {
		
		// Fill the params to be sent.
		Form formData = new Form();
		formData.add("status", status);

		// Make the call to the service.
		com.sun.jersey.api.client.Client client = com.sun.jersey.api.client.Client.create();
		WebResource call = client.resource(srvEndPoint);
		try {
			ClientResponse response = call.path("/aspem/" + Integer.toString(idAspem) + "/status")
					.queryParams(formData).put(ClientResponse.class);
			
			if (response.getStatus() != HTTP_OK) {
				throw new AspemException();
			}
		} catch (Exception ex) {
			throw new AspemException("The ASPEM status could not be notified to the Grid Operator.");
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.aspem.action.GridopServicesCaller#deleteClient(int)
	 */
	public void deleteClient(int idClient) throws AspemException {
		
		// Make the call to the service.
		com.sun.jersey.api.client.Client srvClient = com.sun.jersey.api.client.Client.create();
		WebResource call = srvClient.resource(srvEndPoint);
		try {
			ClientResponse response = call.path("/client/" + Integer.toString(idClient))
					.delete(ClientResponse.class);
			
			if (response.getStatus() != HTTP_OK) {
				throw new AspemException();
			}
		} catch (Exception ex) {
			throw new AspemException("The deletion of the Client entity could not be registered with the Grid Operator.");
		}	
		
	}
	
	
	/**
	 * @see es.siani.aspem.action.GridopServicesCaller#postClient(Client, Aspem)
	 */
	public Client postClient(Client client, Aspem aspem) throws AspemException {
		
		if (client == null) {
			throw new IllegalArgumentException("The parameter 'client' must contain a valid instance of the class Client.");
		}
		
		// TODO-IGN: Refactor all this code.
		// Fill the params to be sent.
		Form formData = new Form();
		formData.add("client", ServicesHelper.ObjectToXML(client));
		//formData.add("aspemGlobalId", aspem.getGlobalId().toString());
		
		// Make the call to the service.
		Client updatedClient;  
		com.sun.jersey.api.client.Client wsClient = com.sun.jersey.api.client.Client.create();
		WebResource call = wsClient.resource(srvEndPoint);
		try {
			ClientResponse response = call.path("/client")
					.type(MediaType.APPLICATION_FORM_URLENCODED)
					.post(ClientResponse.class, formData);
			
			
			if (response.getStatus() == HTTP_CREATED) {
				String xmlClient = response.getEntity(String.class);
				updatedClient = ServicesHelper.XMLToObject(Client.class, xmlClient);
			} else {
				throw new AspemException();
			}
			
		} catch (Exception ex) {
			throw new AspemException("The changes on the ASPEM entity could not be registered with the Grid Operator.", ex);
		}		
		
		return updatedClient;
	}
	
	
	/**
	 * @see es.siani.aspem.action.GridopServicesCaller#getAllClientsGroups()
	 */	
	public List<ClientsGroup> getAllClientsGroups() throws AspemException {
		com.sun.jersey.api.client.Client srvClient = com.sun.jersey.api.client.Client.create();
		WebResource call = srvClient.resource(srvEndPoint);
		
		List<ClientsGroup> groups;
		try {
			ClientResponse response = call.path("/clientsgroup")
					.accept(MediaType.APPLICATION_XML)
					.get(ClientResponse.class);
			
			if (response.getStatus() != HTTP_OK) {
				throw new AspemException();
			}
			groups = response.getEntity(new GenericType<List<ClientsGroup>>() {});
		} catch (Exception ex) {
			throw new AspemException("The Clients Groups could not be retrieved from the Grid Operator.", ex);
		}

		return groups;
	}
	
	
	/**
	 * @see es.siani.aspem.action.GridopServicesCaller#putClient(Client, Aspem)
	 */
	public void putClient(Client client) throws AspemException {

		if (client == null) {
			throw new IllegalArgumentException("The parameter 'client' must contain a valid instance of the class Client.");
		}
		
		// Make the call to the service.
		com.sun.jersey.api.client.Client srvClient = com.sun.jersey.api.client.Client.create();
		WebResource call = srvClient.resource(srvEndPoint);
		try {
			ClientResponse response = call.path("/client/" + Integer.toString(client.getGlobalId()))
					.put(ClientResponse.class, ServicesHelper.ObjectToXML(client));
			
			if (response.getStatus() != HTTP_OK) {
				throw new AspemException();
			}
		} catch (Exception ex) {
			throw new AspemException("The changes on the Client entity could not be registered with the Grid Operator.", ex);
		}		
		
		return;		
	}
	
	
//	/**
//	 * Logger. 
//	 */
//    private static final Logger getLogger() {
//        return Logger.getLogger(GridopServicesCallerImpl.class.getName());
//    }
}
