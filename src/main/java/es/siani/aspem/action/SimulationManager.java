package es.siani.aspem.action;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;

import es.siani.aspem.AspemException;
import es.siani.aspem.data.ClientDataProvider;
import es.siani.aspem.data.DataProvider;
import es.siani.aspem.injection.AspemName;
import es.siani.aspem.model.AsBox;
import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.AspemContainer;
import es.siani.energyagents.EnergyAgentsException;

public class SimulationManager {
	
	protected final ClientDataProvider clientDao;
	protected final AgentsPlatform agentsPlatform;
	protected final DataProvider<AsBox> asboxDao;
	protected final String aspemName;	
	protected AspemContainer brokerContainer;


	/**
	 * Constructor.
	 */
	@Inject
	public SimulationManager(
			@AspemName String aspemName,
			DataProvider<AsBox> asboxDao,
			ClientDataProvider clientDao,
			AgentsPlatform agentsPlatform) {
		
		this.asboxDao = asboxDao;
		this.clientDao = clientDao;
		this.agentsPlatform = agentsPlatform;
		this.aspemName = aspemName;
		this.brokerContainer = null;
		
		return;
	}
	
	
	/**
	 * Actions that must be carried out when a simulation has been started.
	 */
	public void initSimulation(int idSimulation) throws AspemException {
		
		try {
			this.brokerContainer = this.agentsPlatform.createAspemContainer(this.aspemName, null);
		} catch (Throwable t) {
			getLogger().log(Level.SEVERE, "Error during creating the BrokerContainer.");
			throw new AspemException(t);
		}
		
		return;
	}
	
	
	/**
	 * Actions that must be carried out when a simulation has finished.
	 */
	public void finishSimulation(int idSimulation) throws AspemException {
		
		if (this.brokerContainer != null) {
			this.brokerContainer.kill();
		}
		
		return;
	}	
	
	
	/**
	 * Add client to the simulation.
	 */
	public void addAsBox(AsBox asbox, String xmlUserPrefs) throws AspemException {
		
		// Create a Broker Agent and add it to the Container of the ASPEM
		try {
			if (xmlUserPrefs == null) {
				this.brokerContainer.addBrokerAgent(asbox.getName(), null);
				
			} else {
				this.brokerContainer.addBrokerAgent(
						asbox.getName(),
						new StreamSource(new ByteArrayInputStream(xmlUserPrefs.getBytes("UTF-8"))),
						null);
			}
		} catch (EnergyAgentsException ex) {
			getLogger().log(Level.SEVERE, "Error during creating the Broker Agent with the code '" + asbox.getName() + "'.", ex);
			throw new AspemException(ex);
		} catch (UnsupportedEncodingException uex) {
			getLogger().log(Level.SEVERE, "The User preferences are not encoded with UTF-8.", uex);
			throw new AspemException(uex);
		}
		
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SimulationManager.class.getName());
	}
	

}