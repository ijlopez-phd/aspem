package es.siani.aspem.action;

import java.util.List;
import javax.inject.Inject;
import es.siani.aspem.AspemException;
import es.siani.aspem.data.ClientsGroupDataProvider;
import es.siani.aspem.model.ClientsGroup;

public class ClientsGroupManager {
	
	private final ClientsGroupDataProvider groupDao;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public ClientsGroupManager(ClientsGroupDataProvider groupDao) {
		this.groupDao = groupDao;
		
		return;
	}
	
	
	/**
	 * Get all Group items.
	 */
	public List<ClientsGroup> getAllGroups() throws AspemException {
		return this.groupDao.getAll();
	}
	
	
	/**
	 * Get all Group items' codes.
	 */
	public List<String> getAllClientsGroupsCodes() throws AspemException {
		return this.groupDao.getAllClientsGroupsCodes();
	}
	
	
	
	/**
	 * Get all the ClientsGroup that are attached to the specified client.
	 */
	public List<ClientsGroup> getAllAttachedGroups(int idClient) throws AspemException {
		return this.groupDao.getAllAttachedClientsGroupsToClient(idClient);
	}
	
	
	/**
	 * Get a Group item by using the Id.
	 */
	public ClientsGroup getGroupById(int id) throws AspemException {
		return this.groupDao.getById(id);
	}
	
	
	/**
	 * Delete a Group item.
	 */
	public void deleteGroup(int id) throws AspemException {
		// TODIGN: Remove the Group references from all the clients.
		this.groupDao.delete(id);
		
		return;
	}
	
	
	/**
	 * Save a Group item.
	 */
	public void saveGroup(ClientsGroup group) throws AspemException {
		this.groupDao.save(group);
		
		return;
	}

}
