package es.siani.aspem;

import com.vaadin.Application;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.BaseTheme;

import es.siani.aspem.action.ClientManager;
import es.siani.aspem.action.ClientsGroupManager;
import es.siani.aspem.gui.AspemView;
import es.siani.aspem.gui.ClientsGroupsListView;
import es.siani.aspem.gui.ClientsListView;
import es.siani.aspem.gui.TextView;
import es.siani.aspem.model.Aspem;
import static es.siani.aspem.AspemGlobals.*;
import static es.siani.aspem.injection.InjectionUtils.*;

@SuppressWarnings({"serial"})
public class AspemApplication extends Application implements ClickListener {
	
	private static final int IDX_TITLE = 0;
	private static final int IDX_CONTENT = 1;
	
	private static final int LNK_RIGHT = 0;
	private static final int LNK_TOP = 1;
	
	/** List of views of the ASPEM application */
	public static enum Views {
		ASPEM_DESCRIPTION, CLIENTS_LIST, CONTACT_INFO, ABOUT_INFO
	}
	
	// Managers
	private ClientManager clientManager;
	private ClientsGroupManager clientsGroupManager;
	
	// Views
	private AspemView aspemView = null;
	private ClientsListView clientsListView = null;
	private ClientsGroupsListView clientsGroupsListView = null;
	private TextView aboutView = null;
	private TextView contactView = null;
	
	// Links on the left-side bar
	private Button linkAspemView = null;
	private Button linkClientsView = null;
	private Button linkClientsGroupsView = null;
	
	// Links on the top menu
	private Button linkHomeView = null;
	private Button linkAboutView = null;
	private Button linkContactView = null;	
	
	
	/*
	 * Init the application.
	 */
	@Override
	public void init() {
		this.setupInjectors();
		this.setupMainWindow();
		
		return;
	}
	
	
 	/**
	 * Set up the injection dependencies.
	 */
	private void setupInjectors() {
		this.clientManager = injector().getInstance(ClientManager.class);
		this.clientsGroupManager = injector().getInstance(ClientsGroupManager.class);
		
		return;
	}	
	
	
	/**
	 * Read and configure the main window.
	 */
	private void setupMainWindow() {
		this.setTheme("signa");
		
		Window mainWindow = new Window("ASPEM Cloud Application");
		CustomLayout layout = new CustomLayout("signalayout");
		mainWindow.setContent(layout);
		this.setMainWindow(mainWindow);
		
		// Top links
		this.linkHomeView = this.createButtonLinkSection("Home | ", LNK_TOP);
		layout.addComponent(this.linkHomeView, "linkHome");		
		this.linkAboutView = this.createButtonLinkSection("About | ", LNK_TOP);
		layout.addComponent(this.linkAboutView, "linkAbout");
		this.linkContactView = this.createButtonLinkSection("Contact | ", LNK_TOP);
		layout.addComponent(this.linkContactView, "linkContact");
		
		
		// Left-side links
		this.linkAspemView = this.createButtonLinkSection("ASPEM", LNK_RIGHT);
		layout.addComponent(this.linkAspemView, "linkAspemInfo");
		
		this.linkClientsView = this.createButtonLinkSection("Clients", LNK_RIGHT);
		layout.addComponent(this.linkClientsView, "linkClients");
		
		this.linkClientsGroupsView = this.createButtonLinkSection("Groups", LNK_RIGHT);
		layout.addComponent(this.linkClientsGroupsView, "linkClientsGroups");

		// Content of the body
		this.showAspemView();
		
		return;
	}
	
	
	/**
	 * Get the Home view.
	 */
	private Component[] getHomeView() {
		return getAspemView();
	}
	
	
	/**
	 * Get the ASPEM description view.
	 */
	private Component[] getAspemView() {
		if (this.aspemView == null) {
			this.aspemView = new AspemView(this);
		}
		
		return new Component[] {new Label("ASPEM Description"), this.aspemView};
	}
	
	
	/**
	 * Get the clients list view.
	 */
	private Component[] getClientsListView() {
		if (this.clientsListView == null) {
			this.clientsListView = new ClientsListView(this);
		} else {
			this.clientsListView.reloadClientsList();
		}
		
		return new Component[] {new Label("Clients"), this.clientsListView};
	}
	
	
	/**
	 * Get the view of the list of clients groups.
	 */
	private Component[] getClientsGroupsListView() {
		if (this.clientsGroupsListView == null) {
			this.clientsGroupsListView = new ClientsGroupsListView(this);
		}
		
		return new Component[] {new Label("Groups of Clients"), this.clientsGroupsListView};
	}
	
	
	/**
	 * Get the Contact Information view.
	 */
	private Component[] getContactView() {
		if (this.contactView == null) {
			this.contactView = new TextView(CONTACT_RESOURCE);
		}
		
		return new Component[] {new Label("Contact"), this.contactView};
	}
	
	
	/**
	 * Get the About Information view.
	 */
	private Component[] getAboutView() {
		if (this.aboutView == null) {
			this.aboutView = new TextView(ABOUT_RESOURCE);
		}
		
		return new Component[] {new Label("About"), this.aboutView};
	}
	
	
	/**
	 * Show the ASPEM description view.
	 */
	public void showAspemView() {
		this.showBodyView(this.getAspemView());
		return;
	}
	
	
	/**
	 * Show the clients list view.
	 */
	public void showClientsListView() {
		this.showBodyView(this.getClientsListView());
		return;
	}
	
	
	/**
	 * Show the groups clients list view.
	 */
	public void showClientsGroupsListView() {
		this.showBodyView(this.getClientsGroupsListView());
		return;
	}
	
	
	/**
	 * Show the Contact view.
	 */
	public void showContactView() {
		this.showBodyView(this.getContactView());
		return;
	}
	
	
	/**
	 * Show the About view.
	 */
	public void showAboutView() {
		this.showBodyView(this.getAboutView());
		return;
	}
	
	
	/**
	 * Show the Home view.
	 */
	public void showHomeView() {
		this.showBodyView(this.getHomeView());
		return;
	}
	
	
	/**
	 * Show the body components.
	 */
	public void showBodyView(Component[] items) {
		CustomLayout layout = (CustomLayout) this.getMainWindow().getContent();
		layout.addComponent(items[IDX_TITLE], "contentTitle");
		layout.addComponent(items[IDX_CONTENT], "contentBody");
		return;
	}	
	
	/**
	 * Get the Aspem information.
	 */
	public Aspem getAspem() {
		return injector().getInstance(Aspem.class);
	}
	
	/**
	 * Get the data provider for managing the Client item.
	 */
	public ClientManager getClientManager() {
		return this.clientManager;
	}
	
	
	/**
	 * Get the data provider for managing the Group items.
	 */
	public ClientsGroupManager getClientsGroupManager() {
		return this.clientsGroupManager;
	}
	
	
	/**
	 * Handle the events of the links placed at the main window.
	 */
	public void buttonClick(ClickEvent event) {
		Button source = event.getButton();
		if (source == this.linkAspemView) {
			this.showAspemView();
			return;
		} else if (source == this.linkClientsView) {
			this.showClientsListView();
			return;
		} else if (source == this.linkClientsGroupsView) {
			this.showClientsGroupsListView();
			return;
		} else if (source == this.linkAboutView) {
			this.showAboutView();
			return;
		} else if (source == this.linkContactView) {
			this.showContactView();
			return;
		} else if (source == this.linkHomeView) {
			this.showHomeView();
			return;
		}
		
		return;
	}
	
	
	/**
	 * Create a section link 
	 */
	private Button createButtonLinkSection(String caption, int type) {
		Button link = new Button();
		link.setCaption(caption);
		link.setStyleName(BaseTheme.BUTTON_LINK);
		link.addListener(this);		
		if (type == LNK_RIGHT) {
			link.addStyleName("ign-navmenu");
		} else if (type == LNK_TOP) {
			link.addStyleName("ign-topmenu");
		}
		
		return link;
	}	
}
