package es.siani.aspem;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import es.siani.aspem.data.DataProviderHelper;
import es.siani.aspem.model.Aspem;
import static es.siani.aspem.injection.InjectionUtils.*;

public class ApplicationBootstrap implements ServletContextListener {
	
	/**
	 * Application shutdown event.
	 */
	public void contextDestroyed(ServletContextEvent event) {
		return;
	}

	
	/**
	 * Application startup event.
	 */
	public void contextInitialized(ServletContextEvent event) {
		
		// Init the DataProvider.
		DataProviderHelper.initContext(event.getServletContext());
		
		// Read the context params in order to get the code and name of the ASPEM.
		Aspem aspem = injector().getInstance(Aspem.class);
		ServletContext sc = event.getServletContext();
		aspem.setCode(
				(sc.getInitParameter("es.siani.aspem.code") != null)? 
						sc.getInitParameter("es.siani.aspem.code") : sc.getInitParameter("es.siani.aspem.defaultCode"));
		aspem.setName(
				(sc.getInitParameter("es.siani.aspem.name") != null)?
						sc.getInitParameter("es.siani.aspem.name") : sc.getInitParameter("es.siani.aspem.defaultName"));
		
		return;
	}
}
