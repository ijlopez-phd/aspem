package es.siani.aspem.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

import es.siani.aspem.action.GridopServicesCaller;
import es.siani.aspem.action.GridopServicesCallerImpl;
import es.siani.aspem.xmpp.SimpleXmpp;
import es.siani.aspem.xmpp.SimpleXmppImpl;

public class CommInjectionModule extends AbstractModule {

	/**
	 * Dependency Injector module for the Communication objects.
	 */
	@Override
	protected void configure() {
		bind(SimpleXmpp.class).to(SimpleXmppImpl.class).in(Scopes.SINGLETON);
		bind(GridopServicesCaller.class).to(GridopServicesCallerImpl.class).in(Scopes.SINGLETON);
		
		return;
	}	

}
