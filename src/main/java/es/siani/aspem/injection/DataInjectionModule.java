package es.siani.aspem.injection;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;

import es.siani.aspem.action.ClientManager;
import es.siani.aspem.action.ClientsGroupManager;
import es.siani.aspem.action.SimulationManager;
import es.siani.aspem.data.ClientDataProvider;
import es.siani.aspem.data.ClientDataProviderBatisImpl;
import es.siani.aspem.data.ClientsGroupDataProvider;
import es.siani.aspem.data.ClientsGroupDataProviderBatisImpl;
import es.siani.aspem.data.DataProvider;
import es.siani.aspem.data.DataProviderBatisImpl;
import es.siani.aspem.model.AsBox;
import es.siani.aspem.model.Aspem;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;


public class DataInjectionModule extends AbstractModule {
	

	/**
	 * Dependency Injector module for the Manager objects.
	 */	
	@Override
	protected void configure() {
		
		bind(ClientManager.class).in(Scopes.SINGLETON);
		bind(ClientsGroupManager.class).in(Scopes.SINGLETON);
		bind(SimulationManager.class).in(Scopes.SINGLETON);
		
		bind(Aspem.class).in(Scopes.SINGLETON);
		
		return;
	}
	
	/**
	 * Provides the name of the Aspem.
	 */
	@Provides @AspemName
	String getAspemName(Aspem aspem) {
		return aspem.getName();
	}
	
	
	/**
	 * Provider for instancing a DataProvider for the AsBox.
	 */
	@Provides @Singleton
	DataProvider<AsBox> getAsBoxDataProvider() {
		return new DataProviderBatisImpl<AsBox>(AsBox.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an AsBox Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<AsBox> getAsBoxDataProviderBatis() {
		return new DataProviderBatisImpl<AsBox>(AsBox.class);
	}
	
	
	/**
	 * Provider for instancing a ClientsGroup Data Provider. 
	 */
	@Provides @Singleton
	ClientsGroupDataProvider getClientsGroupDataProvider() {
		return new ClientsGroupDataProviderBatisImpl(ClientsGroup.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of a ClientsGroup Data Provider.
	 */
	@Provides @Singleton
	ClientsGroupDataProviderBatisImpl getClientsGroupDataProviderBatis() {
		return new ClientsGroupDataProviderBatisImpl(ClientsGroup.class);
	}
	
	
	/**
	 * Provider for instancing a ClientDataProvider.
	 */
	@Provides @Singleton
	ClientDataProvider getClientDataProvider() {
		return new ClientDataProviderBatisImpl(Client.class);
	}

	
	/**
	 * Provider for instancing a Batis implementation of a ClientDataProvider.
	 */
	@Provides @Singleton
	ClientDataProviderBatisImpl getClientDataProviderBatisImpl() {
		return new ClientDataProviderBatisImpl(Client.class);
	}
}