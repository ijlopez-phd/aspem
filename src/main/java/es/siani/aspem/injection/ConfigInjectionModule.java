package es.siani.aspem.injection;

import java.util.logging.Logger;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import es.siani.aspem.config.Configuration;
import es.siani.aspem.config.ConfigurationLoader;


public class ConfigInjectionModule extends AbstractModule {
	
	/**
	 * Dependency Injector module for the Configuration object.
	 */
	@Override
	protected void configure() {
		return;
	}


	/**
	 * Provider for instancing the Configuration object.
	 */
	@Provides
	Configuration configurationLoader() {
		ConfigurationLoader loader = new ConfigurationLoader();
		try {
			return loader.getConfiguration();
		} catch (IllegalArgumentException argex) {
			try {
				getLogger().severe("The proposed configuration file couldn't be loaded. Loading the internal configuration file.");
				return loader.getConfiguration();
			} catch (IllegalArgumentException innerargex) {
				getLogger().severe("No configuration file has been loaded.");
			}
		}

		return null;
	}


	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ConfigInjectionModule.class.getName());
	}

}
