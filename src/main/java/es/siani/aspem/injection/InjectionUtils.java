package es.siani.aspem.injection;

import com.google.inject.Guice;
import com.google.inject.Injector;

import es.siani.energyagents.injection.BehavioursExp01Module;
import es.siani.energyagents.injection.BehavioursExp03Module;
import es.siani.energyagents.injection.BehavioursExp04Module;
import es.siani.energyagents.injection.EnergyAgentsModule;
import es.siani.energyagents.injection.PlatformExp01Module;
import es.siani.energyagents.injection.PlatformExp03Module;
import es.siani.energyagents.injection.PlatformExp04Module;

public class InjectionUtils {
	
	private static Injector _injector = null;
	
	/**
	 * Constructor.
	 */
	private InjectionUtils() {
		return;
	}
	
	
	/**
	 * Return an Injector.
	 */
	public static Injector injector() {
		
		if (_injector == null) {
			_injector = Guice.createInjector(
					new ConfigInjectionModule(),
					new DataInjectionModule(),
					new CommInjectionModule(),
					new EnergyAgentsModule(),
					new PlatformExp04Module(),
					new BehavioursExp04Module(),
					new es.siani.energyagents.injection.ConfigModule());
		}
		
		return _injector;		
	}

}
