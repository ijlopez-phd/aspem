package es.siani.aspem.data;

import es.siani.aspem.AspemException;

@SuppressWarnings("serial")
public class AspemDataException extends AspemException {

	/**
	 * Constructor.
	 */
	public AspemDataException() {
		super();
	}

	
	/**
	 * Constructor.
	 */
	public AspemDataException(String message, Throwable cause) {
		super(message, cause);
	}

	
	/**
	 * Constructor.
	 */
	public AspemDataException(String message) {
		super(message);
	}

	
	/**
	 * Constructor.
	 */
	public AspemDataException(Throwable cause) {
		super(cause);
	}
}
