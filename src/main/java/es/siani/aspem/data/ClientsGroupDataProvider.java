package es.siani.aspem.data;

import java.util.List;
import java.util.Set;

import es.siani.aspem.model.ClientsGroup;

public interface ClientsGroupDataProvider extends DataProvider<ClientsGroup> {
	
	/**
	 * Get the codes of all the clients groups.
	 * @throws AspemDataException
	 */
	public List<String> getAllClientsGroupsCodes() throws AspemDataException;


	/**
	 * Get all the clients groups that are attached to the specified client.
	 * @throws AspemDataException 
	 */
	public List<ClientsGroup> getAllAttachedClientsGroupsToClient(int idClient) throws AspemDataException;
	
	
	/**
	 * Update the clients groups to which the client belongs.
	 */
	public void setClientsGroups(int idClient, Set<ClientsGroup> clientsGroup) throws AspemDataException;
}