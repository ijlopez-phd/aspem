package es.siani.aspem.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.ibatis.session.SqlSession;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;

public class ClientDataProviderBatisImpl extends DataProviderBatisImpl<Client> implements ClientDataProvider  {
	
	
	/**
	 * Constructor.
	 */
	public ClientDataProviderBatisImpl(Class<Client> dataClass) {
		super(dataClass);
		return;
	}
	
	
	/**
	 * Save a Client item, including the relationship it has with the ClientsGroup entities.
	 */
	@Override
	public void save(Client item) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			if (item.getId() == null) {
				// The item hasn't been stored yet.
				session.insert("insertClient", item);
			} else {
				// The item is being updated.
				session.update("updateClient", item);
				session.delete("removeClientFromAllClientsGroups", item.getId());
			}
			
			Map<String, Object> params = new HashMap<String, Object>();				
			params.put("clientId", item.getId());
			for (ClientsGroup group : item.getClientsGroups()) {
				params.put("clientsGroupId", group.getId());
				session.insert("addClientToClientsGroup", params);
				params.remove("clientsGroupId");
			}			
			
			session.commit();
		} catch (Exception ex) {
			getLogger().severe("The item could not be saved.");
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.aspem.data.ClientDataProvider#getClientsGroup(int)
	 */
	public List<ClientsGroup> getClientsGroups(int idClient) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		List<ClientsGroup> groups;
		try {
			groups = session.selectList("getClientsGroupsOfClient", idClient);
		} catch (Exception ex) {
			getLogger().severe("Clients groups could not be retrieved from the database for the specified client.");
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return groups;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ClientDataProviderBatisImpl.class.getName());
	}

}
