package es.siani.aspem.data;

public interface DataProviderNotifier<T> {
	
	/**
	 * Add a listener to the data provider implementing this interface.
	 */
	public void addListener(DataProviderListener<T> listener);
	
	
	/**
	 * Notify to all the listeners.
	 */
	public void notifyItemAction(int actionType, T item);

}
