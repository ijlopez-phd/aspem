package es.siani.aspem.data;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;

import com.google.inject.Inject;

import es.siani.aspem.model.BaseItem;


public class DataProviderBatisImpl<T extends BaseItem> implements DataProvider<T> {
	
	/** List of objects to be notified about data actions. */
	private List<DataProviderListener<T>> listeners = new ArrayList<DataProviderListener<T>>();
	
	/** Name of the specialized class */
	final String itemName;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public DataProviderBatisImpl(Class<T> dataClass) {
		this.itemName = dataClass.getSimpleName();
		return;
	}

	
	/**
	 * Get all items.
	 */
	public List<T> getAll() throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		List<T> items;
		try {
			items = session.selectList("getAll" + itemName + "s");
		} catch (Exception ex) {
			getLogger().severe("Items could not be retrieved from the database.");			
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return items;
	}

	
	/**
	 * Get the first item.
	 */
	public T getFirst() throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		T item = null;
		try {
			List<T> items = session.selectList("getFirst" + itemName, null, new RowBounds(0, 1));
			if ((items != null) && (items.size() > 0)) {
				item = items.get(0);
			}
		} catch (Exception ex) {
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return item;
	}

	
	/**
	 * Get item by Id.
	 */
	@SuppressWarnings("unchecked")
	public T getById(int id) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		T item;
		try {
			item = (T)session.selectOne("get" + itemName + "ById", id);
			if (item == null) {
				throw new AspemDataException("There is not " + itemName + " with id equal to " + id);
			}
		} catch (Exception ex) {
			getLogger().severe("The " + itemName + " with id " + id + " couldn't be retrieved.");			
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return item;
	}

	
	/**
	 * Get item by Code.
	 */
	@SuppressWarnings("unchecked")
	public T getByCode(String code) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		T item;
		try {
			item = (T)session.selectOne("get" + itemName + "ByCode", code);
			if (item == null) {
				throw new AspemDataException("There is not " + itemName + " with code equal to " + code);
			}
		} catch (Exception ex) {
			getLogger().severe("The " + itemName + " with code " + code + " couldn't be retrieved.");			
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return item;	
	}

	
	/**
	 * Delete an item.
	 */
	public void delete(int id) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			session.delete("delete" + itemName, id);
			session.commit();
		} catch (Exception ex) {
			getLogger().severe("The " + itemName + " with id " + id + " couldn't be deleted.");
			throw new AspemDataException(ex);			
		} finally {
			session.close();
		}
		
		return;
		
	}

	
	/**
	 * Save an item.
	 */
	public void save(T item) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			if (item.getId() == null) {
				// The item hasn't been stored yet.
				session.insert("insert" + this.itemName, item);
			} else {
				// The item is being updated.
				session.update("update" + this.itemName, item);
			}
			session.commit();
			
		} catch (Exception ex) {
			getLogger().severe("The item could not be saved.");			
			throw new AspemDataException(ex);

		} finally {
			session.close();
		}
		
		return;
		
	}

	
	/**
	 * Add a listener to the data provider.
	 */
	public void addListener(DataProviderListener<T> listener) {
		this.listeners.add(listener);
		return;
	}

	
	/**
	 * Notify to all the listeners about data actions.
	 */	
	public void notifyItemAction(int actionType, T item) {
		for (DataProviderListener<T> listener : this.listeners) {
			listener.itemChanged(actionType, item);
		}
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(DataProviderBatisImpl.class.getName());
	}

}
