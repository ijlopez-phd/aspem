package es.siani.aspem.data;

import java.util.List;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;

public interface ClientDataProvider extends DataProvider<Client> {
	
	/**
	 * Get the groups to which the client belongs.
	 */
	public List<ClientsGroup> getClientsGroups(int idClient) throws AspemDataException;
}