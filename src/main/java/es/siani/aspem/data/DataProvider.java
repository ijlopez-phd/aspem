package es.siani.aspem.data;

import java.util.List;

public interface DataProvider<T> {
	
	public List<T> getAll() throws AspemDataException;
	
	public T getFirst() throws AspemDataException;
	
	public T getById(int id) throws AspemDataException;
	
	public T getByCode(String code) throws AspemDataException;
	
	public void delete(int id) throws AspemDataException;
	
	public void save(T item) throws AspemDataException;
	
	public void addListener(DataProviderListener<T> listener);
	
	public void notifyItemAction(int actionType, T item);

}
