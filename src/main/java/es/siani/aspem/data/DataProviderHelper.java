package es.siani.aspem.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


public class DataProviderHelper {
	
	private static SqlSessionFactory sqlSession = null;
	private static ServletContext context = null;
	
	
	/**
	 * Constructor.
	 * This class cannot be instantiated.
	 */
	private DataProviderHelper() {
		return;
	}
	
	
	/**
	 * Set the ServletContext of the Web Application.
	 */
	public static void initContext(ServletContext servletContext) {
		context = servletContext;
		return;
	}
	

	/**
	 * Get the SqlSessionFactory instance.
	 * This is used by MyBatis in order to run the sql statemens.
	 */
	public static SqlSessionFactory getSQLSessionFactory() {
		if (sqlSession == null) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						context.getResourceAsStream("/WEB-INF/mybatis/config.xml")));
				sqlSession = new SqlSessionFactoryBuilder().build(reader);
			} catch(Exception ex) {
				ex.printStackTrace();
				getLogger().severe("The SqlSessionFactory of mybatis could not be created.");
			}
		}
		
		return sqlSession;
	}
	
	
	/**
	 * Logger.
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(DataProviderHelper.class.getName());
    }	
}
