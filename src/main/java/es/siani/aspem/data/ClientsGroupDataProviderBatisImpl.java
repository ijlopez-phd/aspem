package es.siani.aspem.data;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.ibatis.session.SqlSession;

import es.siani.aspem.model.ClientsGroup;

public class ClientsGroupDataProviderBatisImpl extends DataProviderBatisImpl<ClientsGroup> implements ClientsGroupDataProvider {
	
	
	/**
	 * Constructor.
	 */
	public ClientsGroupDataProviderBatisImpl(Class<ClientsGroup> dataClass) {
		super(dataClass);
		return;
	}
	
	
	/**
	 * @see es.siani.aspem.data.ClientsGroupDataProvider#getAllClientsGroupsCodes()
	 */
	public List<String> getAllClientsGroupsCodes() throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		List<String> groupsCodes;
		try {
			groupsCodes = session.selectList("getAllClientsGroupsCodes");
		} catch (Exception ex) {
			getLogger().severe("The codes of the clients groups could not be retrieved from the database.");
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return groupsCodes;
	}
	
	
	/**
	 * @see es.siani.aspem.data.ClientsGroupDataProvider#getAllAttachedClientsGroupsToClient(int)
	 */
	public List<ClientsGroup> getAllAttachedClientsGroupsToClient(int idClient) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		List<ClientsGroup> groups;
		try {
			groups = session.selectList("getAllAttachedClientsGroupsToClient", idClient);
		} catch (Exception ex) {
			getLogger().severe("Clients groups could not be retrieved from the database for the specified client.");
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return groups;
	}
	
	
	/**
	 * @see es.siani.aspem.data.ClientsGroupDataProvider#setClientsGroups(int, Set<ClientsGroup>)
	 */	
	public void setClientsGroups(int idClient, Set<ClientsGroup> clientsGroup) throws AspemDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			
		} catch (Exception ex) {
			getLogger().severe("Clients groups could not be set.");
			throw new AspemDataException(ex);
		} finally {
			session.close();
		}
		
		return;
		
	}	
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ClientsGroupDataProviderBatisImpl.class.getName());
	}
}
