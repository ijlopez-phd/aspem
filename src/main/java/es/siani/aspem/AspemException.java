package es.siani.aspem;

@SuppressWarnings("serial")
public class AspemException extends Exception {
	
	/**
	 * Constructor.
	 */
	public AspemException() {
		super();
	}

	
	/**
	 * Constructor.
	 */
	public AspemException(String message, Throwable cause) {
		super(message, cause);
	}

	
	/**
	 * Constructor.
	 */
	public AspemException(String message) {
		super(message);
	}

	
	/**
	 * Constructor.
	 */
	public AspemException(Throwable cause) {
		super(cause);
	}
}
