package es.siani.aspem.gui;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.TextField;

import es.siani.aspem.AspemApplication;
import es.siani.aspem.model.Aspem;

@SuppressWarnings("serial")
public class AspemView extends Form {
	
	// Visible properties of the form
	public static final String visibleProperties[] = {"code", "name"};
	
	
	/**
	 * Constructor.
	 * @param app
	 */
	public AspemView(final AspemApplication app) {
		
		/*
		 * Disable buffering so that input is written immediately
		 * in the underlying object. 
		 */
		this.setWriteThrough(false);
		
		// Set the field factory
		this.setFormFieldFactory(new DefaultFieldFactory() {
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				
				String propName = propertyId.toString();
				
				if (propName.equalsIgnoreCase("CODE")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Code");
					field.setDescription("Code of the ASPEM");
					field.setNullRepresentation("");
					field.setReadOnly(true);
					return field;					

				} else if (propName.equalsIgnoreCase("NAME")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Name");
					field.setDescription("Name of the ASPEM");
					field.setNullRepresentation("");
					field.setReadOnly(true);
					return field;

				}

				return null;
			}
		});
		
		
		// Attach the data source to the form
		Aspem aspem = app.getAspem();
		this.setItemDataSource(new BeanItem<Aspem>(aspem));
		
		// Set the visible properties of the data source.
		this.setVisibleItemProperties(AspemView.visibleProperties);
		
		return;
	}
	
	
//	/**
//	 * Logger.
//	 */
//	private static final Logger getLogger() {
//		return Logger.getLogger(Aspem.class.getName());
//	}
}
