package es.siani.aspem.gui;

import java.util.List;
import java.util.logging.Logger;
import org.vaadin.dialogs.ConfirmDialog;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;
import es.siani.aspem.AspemApplication;
import es.siani.aspem.AspemException;
import es.siani.aspem.data.DataProviderListener;
import es.siani.aspem.model.Client;
import static es.siani.aspem.AspemGlobals.*;


@SuppressWarnings("serial")
public class ClientsListView
		extends Panel 
		implements ClickListener, Table.ValueChangeListener, Action.Handler, ConfirmDialog.Listener, DataProviderListener<Client> {
	
	// Natural property order for Client data (used in tables and forms)
	public static final Object[] NATURAL_COL_ORDER = new Object[] {
		"code", "alias", "xmppAddress"
	};
	
	// Human readable captions for properties (in same order as NATURAL_COL_ORDER)
	public static final String[] COL_CAPTIONS = new String[] {
		"Code", "Alias", "XMPP Address"
	};
	
	// Context-menu actions of the table
	private static Action ACTION_NEW = new Action("New");
	private static Action ACTION_VIEW = new Action("View");	
	private static Action ACTION_EDIT = new Action("Edit");
	private static Action ACTION_DELETE = new Action("Delete");
	private static Action TABLE_ITEM_ACTIONS[] =
		{ACTION_NEW, ACTION_VIEW, ACTION_EDIT, ACTION_DELETE};
	
	// Table actions buttons
	private final Button buttonView = new Button("View", (ClickListener)this);
	private final Button buttonNew = new Button("New", (ClickListener)this);
	private final Button buttonRemove = new Button("Remove", (ClickListener)this);
	private final Button buttonEdit = new Button("Edit", (ClickListener)this);
	private final Button buttonUpload = new Button("Upload", (ClickListener)this);

	/** Reference to the main application object */
	private final AspemApplication app;
	
	/** Table that lists all clients */
	private final Table clientsTable = new Table();	
	
	
	/**
	 * Constructor.
	 */
	public ClientsListView(final AspemApplication app) {
		super();
		
		this.app = app;
		
		// Add this view as listener of the Client data provider
		this.app.getClientManager().addListener(this);
		
		// Populate the table's container with the registered Clients
		BeanItemContainer<Client> container = new BeanItemContainer<Client>(Client.class);
		try {
			List<Client> clients = this.app.getClientManager().getAllClients();
			container.addAll(clients);
		} catch (AspemException ex) {
			getLogger().severe("Trying to get the list of clients of the database.");
		}
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the Clients view		
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		// Table with all clients
		this.clientsTable.setSizeFull();
		this.clientsTable.setContainerDataSource(container);
		this.clientsTable.setVisibleColumns(NATURAL_COL_ORDER);
		this.clientsTable.setColumnHeaders(COL_CAPTIONS);
		this.clientsTable.setSelectable(true);
		this.clientsTable.setImmediate(true);
		this.clientsTable.setColumnReorderingAllowed(true);
		this.clientsTable.setMultiSelect(false);
		this.clientsTable.setPageLength(TABLE_MAX_VISIBLE_ITEMS);
		this.clientsTable.addListener((Table.ValueChangeListener)this);
		this.clientsTable.addActionHandler(this);
		layout.addComponent(this.clientsTable);
		
		// Buttons for actions
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setMargin(new MarginInfo(true, false, true, false));
		buttonsLayout.addComponent(this.buttonView);
		buttonsLayout.addComponent(this.buttonNew);
		buttonsLayout.addComponent(this.buttonEdit);
		buttonsLayout.addComponent(this.buttonRemove);
		buttonsLayout.addComponent(this.buttonUpload);
		layout.addComponent(buttonsLayout);
		
		// Some buttons are not enabled until a row is selected
		this.enableButtons(false);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;
	}
	
	
	/**
	 * Reload the list of clients.
	 */
	@SuppressWarnings("unchecked")
	public void reloadClientsList() {
		
		BeanItemContainer<Client> container = (BeanItemContainer<Client>)this.clientsTable.getContainerDataSource();
		
		try {
			List<Client> clients = this.app.getClientManager().getAllClients();
			container.removeAllItems();
			container.addAll(clients);
		} catch (AspemException ex) {
			getLogger().severe("The list of clients could not be read.");
		}
		
		return;
	}
	
	
	/**
	 * Show the popup that allows inserting a new client.
	 */
	private void doNewClientAction() {
		ClientPopup popup = new ClientPopup(
				this.app,
				this,
				DisplayModeEnum.NEW,
				new BeanItem<Client>(new Client()));
		this.app.getMainWindow().addWindow(popup);
		
		return;
	}
	
	
	/**
	 * Show the popup that allows editing a client.
	 */
	private void doEditClientAction() {
		Client client = (Client)this.clientsTable.getValue();		
		if (client != null) {
			ClientPopup popup = new ClientPopup(
					this.app,
					this,
					DisplayModeEnum.EDIT,
					this.clientsTable.getContainerDataSource().getItem(client));
			this.app.getMainWindow().addWindow(popup);
		}
		
		return;
	}
	
	
	/**
	 * Show the popup within the information of a client.
	 */
	private void doViewClientAction() {
		Client client = (Client)this.clientsTable.getValue();
		if (client != null) {
			ClientPopup popup = new ClientPopup(
					this.app,
					this,
					DisplayModeEnum.READ,
					new BeanItem<Client>(client));
			this.app.getMainWindow().addWindow(popup);
		}
		
		return;
	}
	
	
	/**
	 * Show the dialog that request to confirm the 'Delete' action.
	 */
	private void doRemoveClientAction() {
		Client client = (Client)this.clientsTable.getValue();
		if (client != null) {
			ConfirmDialog.show(
					this.app.getMainWindow(),
					"Please Confirm:",
					"Are you really sure?",
					"I am", "Not quite",
					(ConfirmDialog.Listener)this);			
		}

		return;
	}	
	
	
	/**
	 * Handle the click events related to the action buttons.
	 */
	public void buttonClick(ClickEvent event) {
		
		Button source = event.getButton();
		if (source == this.buttonView) {
			this.doViewClientAction();
			
		} else if (source == this.buttonNew) {
			this.doNewClientAction();
			
		} else if (source == this.buttonEdit) {
			this.doEditClientAction();
			
		} else if (source == this.buttonRemove) {
			this.doRemoveClientAction();
		}
		
		return;
	}
	
	
	/**
	 * Handle changed selected items on the table. 
	 */
	public void valueChange(ValueChangeEvent event) {
		boolean enabled = (event.getProperty().getValue() != null)? true : false;
		this.enableButtons(enabled);
		
		return;
	}


	/**
	 * Return the list of actions of the table's context-menu.
	 */
	public Action[] getActions(Object target, Object sender) {
		return TABLE_ITEM_ACTIONS;
	}


	/**
	 * Handle click events on the table's context-menu.
	 */
	public void handleAction(Action action, Object sender, Object target) {
		
		this.clientsTable.select(target);
		
		if ((action == ACTION_VIEW) && (target != null)) {
			this.doViewClientAction();
			
		} else if (action == ACTION_NEW) {
			this.doNewClientAction();
			
		} else if ((action == ACTION_EDIT) && (target !=null)) {
			this.doEditClientAction();
			
		} else if ((action == ACTION_DELETE) && (target != null)) {
			this.doRemoveClientAction();
		}
		
		return;
	}


	/**
	 * Handle the closing event of the confirm dialog.
	 */
	public void onClose(ConfirmDialog dialog) {
		if (dialog.isConfirmed()) {
			try {
				Client client = (Client)this.clientsTable.getValue();
				this.app.getClientManager().deleteClient(client);
				this.clientsTable.getContainerDataSource().removeItem(client);
				this.enableButtons(false);
				
			} catch (AspemException ex) {
				getLogger().severe("The Client item could not be deleted.");
			}
		}
		
		return;
	}
	
	
	/**
	 * Enable or disable buttons depending on a row is selected.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonView.setEnabled(enabled);
		this.buttonEdit.setEnabled(enabled);
		this.buttonRemove.setEnabled(enabled);
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ClientsListView.class.getName());
	}
	
	
	/**
	 * Called when an item action is performed on the Client Data Provider.
	 */
	public void itemChanged(int actionType, Client item) {
		
		Container container = this.clientsTable.getContainerDataSource();
		if (actionType == DataProviderListener.ASPEM_ITEM_SAVED) {
			// A new client has been created. It is added to the table's container.
			container.addItem(item);
		}
		
		return;
	}	
}
