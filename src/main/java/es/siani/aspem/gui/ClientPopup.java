package es.siani.aspem.gui;

import java.util.List;
import java.util.logging.Logger;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.Window;
import es.siani.aspem.AspemApplication;
import es.siani.aspem.AspemException;
import es.siani.aspem.action.ClientManager;
import es.siani.aspem.model.Client;
import es.siani.aspem.model.ClientsGroup;


@SuppressWarnings("serial")
public class ClientPopup extends Window implements ClickListener, Window.CloseListener {
	
	
	/** Visible properties of the form */
	public static final String visiblePropertiesNew[] = {"code", "alias", "clientsGroups"};
	public static final String visiblePropertiesReadEdit[] = {"code", "alias", "xmppAddress", "remoteXmppAddress", "clientsGroups"};
	
	// Actions buttons
	private final Button buttonSave = new Button("Save", (ClickListener)this);
	private final Button buttonCancel = new Button("Cancel", (ClickListener)this);
	private final Button buttonOk = new Button("Ok", (ClickListener)this);
	
	/** Main application object */
	private final AspemApplication app;
	
	/** Parent view: clients list */
	private final ClientsListView parentView;
	
	/** Form with the data fields of the client */
	private final Form form;
 
	
	/**
	 * Constructor.
	 */
	public ClientPopup(AspemApplication app, ClientsListView parentView,final DisplayModeEnum mode, Item itemDataSource) {
		super("Client Information");
		
		this.app = app;
		this.parentView = parentView;
		
		// Size and position of the popup
		this.center();
		this.getContent().setSizeUndefined();
		this.setResizable(false);

		
		// Build the form and the input fields
		this.form = new Form();
		this.form.setSizeUndefined();
		this.form.getLayout().setSizeUndefined();
		this.form.setWriteThrough(true);
		final boolean readOnlyMode = (mode == DisplayModeEnum.READ)? true : false;
		
		// Set the field factory of the form
		this.form.setFormFieldFactory(new DefaultFieldFactory() {
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				
				String propName = propertyId.toString();
				
				if (propName.equalsIgnoreCase("CODE")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Code");
					field.setDescription("Unique token that identifies the client.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 256 characters.", 1, 256, false));
					field.setReadOnly(readOnlyMode);
					return field;
					
				} else if (propName.equalsIgnoreCase("ALIAS")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Alias");
					field.setDescription("Alias related to the client");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 128 characters.", 1, 128, false));
					field.setReadOnly(readOnlyMode);
					return field;
					
				} else if ((propName.equalsIgnoreCase("XMPPADDRESS")) && (mode != DisplayModeEnum.NEW)) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Broker XMPP");
					field.setDescription("XMPP address related to the client.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 512 characters.", 1, 512, false));
					field.setReadOnly(true);
					return field;
					
				} else if ((propName.equalsIgnoreCase("REMOTEXMPPADDRESS")) && (mode != DisplayModeEnum.NEW)) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("AS-Box XMPP");
					field.setDescription("XMPP address related to the ESI to which is connected the client.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 512 characters.", 1, 512, false));
					field.setReadOnly(true);
					return field;					
					
				} else if (propName.equalsIgnoreCase("CLIENTSGROUPS")) {
					TwinColSelect field = new TwinColSelect("Clients groups");
					field.setCaption("Groups");
					field.setDescription("Groups to which the client belongs.");
					field.setRows(8);
					field.setReadOnly(readOnlyMode);
					field.setLeftColumnCaption("Available");
					field.setRightColumnCaption("Selected");
					final ClientManager clientManager = ClientPopup.this.app.getClientManager();
							
					try {
						final List<ClientsGroup> availableGroups = clientManager.getAllClientsGroups();
						field.setContainerDataSource(
								new BeanItemContainer<ClientsGroup>(ClientsGroup.class, availableGroups));
						field.setItemCaptionMode(TwinColSelect.ITEM_CAPTION_MODE_PROPERTY);
						field.setItemCaptionPropertyId("code");
						
						if (mode == DisplayModeEnum.EDIT) {
							final List<ClientsGroup> selectedGroups = 
									clientManager.getClientsGroups((Integer)item.getItemProperty("id").getValue());
							field.setValue(selectedGroups);
						}

					} catch (AspemException ex) {
						getLogger().severe("The Clients Group could not be read from the database.");
						return field;
					}
					
					return field; 
				}
				
				return null;
			}
		});
		

		this.form.setItemDataSource(itemDataSource);
		if (mode == DisplayModeEnum.NEW) {
			this.form.setVisibleItemProperties(ClientPopup.visiblePropertiesNew);
		} else {
			this.form.setVisibleItemProperties(ClientPopup.visiblePropertiesReadEdit);
		}

		// Buttons
		HorizontalLayout footerLayout = new HorizontalLayout();
		footerLayout.addStyleName("ign-buttonsbar");
		footerLayout.setWidth("100%");
		Panel buttonsPanel = new Panel();
		buttonsPanel.setSizeUndefined();
		buttonsPanel.addStyleName("ign-buttonswrap");
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setSizeUndefined();
		buttonsPanel.setContent(buttonsLayout);
		if ((mode == DisplayModeEnum.NEW) || (mode == DisplayModeEnum.EDIT)) {
			buttonsPanel.addComponent(this.buttonCancel);
			buttonsPanel.addComponent(this.buttonSave);
		} else if (mode == DisplayModeEnum.READ){
			buttonsPanel.addComponent(this.buttonOk);
		}
		
		footerLayout.addComponent(buttonsPanel);
		footerLayout.setComponentAlignment(buttonsPanel, Alignment.MIDDLE_RIGHT);
		this.form.setFooter(footerLayout);
		
		
		this.getContent().addComponent(form);
		
		return;
	}
	
	
	/**
	 * Handle the click events related to the buttons.
	 */
	@SuppressWarnings("unchecked")
	public void buttonClick(ClickEvent event) {
		
		final Button source = event.getButton();
		
		if (source == this.buttonSave) {
			try {
				// Commit the changes from the Form to the Item
				this.form.commit();
				// Commit the changes to the database
				BeanItem<Client> item = (BeanItem<Client>)this.form.getItemDataSource();
				this.app.getClientManager().createClient(item.getBean());
				this.parentView.reloadClientsList();
			} catch (AspemException ex) {
				getLogger().severe("The Client data could not be stored in the database.");
			}
			this.close();
			
		} else if (source == this.buttonCancel) {
			// Discard changes since last commit
			this.form.discard();
			this.close();
			
		} else if (source == this.buttonOk) {
			this.close();
		}
		
		return;
	}


	/**
	 * Handle the close window event.
	 */
	public void windowClose(CloseEvent e) {
		this.form.discard();
	}
	
	
	/**
	 * Logger. 
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(ClientPopup.class.getName());
    }
}
