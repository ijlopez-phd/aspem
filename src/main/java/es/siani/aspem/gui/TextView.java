package es.siani.aspem.gui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;


@SuppressWarnings("serial")
public class TextView extends Panel {
	
	
	/**
	 * Constructor.
	 */
	public TextView(String resourceName) {
		super();
		
		// Read the text
		String content = this.readContextText(resourceName);
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the view
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		Label label = new Label(content);
		label.setContentMode(Label.CONTENT_XHTML);
		layout.addComponent(label);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;
	}
	
	
	/**
	 * Read the text to be showed in the panel.
	 */
	private String readContextText(String resourceName) {
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		StringBuffer buffer = new StringBuffer();
		try  {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(classLoader.getResourceAsStream(resourceName)));
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				buffer.append(line);
			}
		} catch (Exception ex) {
			final String message = "The resource '" + resourceName + "' cannot be read. Does it exist?";
			getLogger().severe(message);
			throw new IllegalArgumentException(message);
		}
		
		return buffer.toString();
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(TextView.class.getName());
	}		
}
