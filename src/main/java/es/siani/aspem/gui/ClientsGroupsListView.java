package es.siani.aspem.gui;

import static es.siani.aspem.AspemGlobals.TABLE_MAX_VISIBLE_ITEMS;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

import es.siani.aspem.AspemApplication;
import es.siani.aspem.AspemException;
import es.siani.aspem.model.ClientsGroup;


@SuppressWarnings("serial")
public class ClientsGroupsListView 
		extends Panel
		implements ClickListener, Handler, ConfirmDialog.Listener, ValueChangeListener {
	
	// Natural property order for the Group data
	private static final Object[] NATURAL_COL_ORDER = new Object[] {
		"code", "name"
	};
	
	// Human redeable captions for properties (in same order as NATURAL_COL_ORDER)
	private static final String[] COL_CAPTIONS = new String[] {
		"Code", "Name"
	};
	
	/** Reference to the main application object */
	private final AspemApplication app;
	
	/** Table that lists all the scenarios registered with the Grid Operator System */
	private final Table groupsTable = new Table();
	
	// Table's action buttons.
	private final Button buttonView = new Button("View", (ClickListener)this);
	
	// Context-menu's actions.
	private static Action ACTION_VIEW = new Action("View");
	private static Action TABLE_ITEM_ACTIONS[] = {ACTION_VIEW}; 
	
	
	/**
	 * Constructor.
	 */
	public ClientsGroupsListView(final AspemApplication app) {
		super();
		
		this.app = app;
		
		// Populate the table's container with the registered Groups.
		BeanItemContainer<ClientsGroup> container = new BeanItemContainer<ClientsGroup>(ClientsGroup.class);
		try {
			List<ClientsGroup> groups = this.app.getClientManager().getAllClientsGroups();
			container.addAll(groups);
		} catch (AspemException ex) {
			getLogger().severe("The list of groups could not be read from the database.");
		}
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the view
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		// Table with all groups
		this.groupsTable.setSizeFull();
		this.groupsTable.setContainerDataSource(container);
		this.groupsTable.setVisibleColumns(NATURAL_COL_ORDER);
		this.groupsTable.setColumnHeaders(COL_CAPTIONS);		
		this.groupsTable.setSelectable(true);
		this.groupsTable.setImmediate(true);
		this.groupsTable.setColumnReorderingAllowed(false);
		this.groupsTable.setMultiSelect(false);
		this.groupsTable.setPageLength(TABLE_MAX_VISIBLE_ITEMS);
		this.groupsTable.addListener((Table.ValueChangeListener)this);
		this.groupsTable.addActionHandler(this);
		this.groupsTable.setColumnExpandRatio("code", 1f);
		this.groupsTable.setColumnExpandRatio("name", 3f);
		layout.addComponent(this.groupsTable);
		
		
		// Buttons for actions
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(new MarginInfo(true, false, true, false));
		buttonsLayout.setSpacing(true);
		buttonsLayout.addComponent(this.buttonView);
		layout.addComponent(buttonsLayout);
		
		// Some buttons are not enabled until a row is selected
		this.enableButtons(false);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;
	}
	
	
	/**
	 * Reload the list of clients groups.
	 */
	@SuppressWarnings("unchecked")
	public void reloadList() {
		
		BeanItemContainer<ClientsGroup> container = (BeanItemContainer<ClientsGroup>)this.groupsTable.getContainerDataSource();
		try {
			List<ClientsGroup> groups = this.app.getClientManager().getAllClientsGroups();
			container.removeAllItems();
			container.addAll(groups);
		} catch (AspemException ex) {
			getLogger().log(Level.SEVERE, "The list of clients groups could not be refreshed.", ex);
		}
		
		return;
	}
	
	
	/**
	 * Show the form for inserting a new Group.
	 */
	private void doViewGroupAction() {
		
		Object selectedValue = this.groupsTable.getValue();
		if (selectedValue == null) {
			return;
		}
		
		try {
			ClientsGroupView popup = new ClientsGroupView(
//					this.app,
//					this,
					DisplayModeEnum.READ,
					this.groupsTable.getContainerDataSource().getItem(selectedValue));
			this.app.getMainWindow().addWindow(popup);
		} catch (Exception ex) {
			getLogger().severe("The selected item cound not be read.");
		}
		
		return;
	}
	
	
	/**
	 * Enable or disable buttons depending on a row is selected.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonView.setEnabled(enabled);
		
		return;
	}
	

	/**
	 * Handle changes on selected items of the table.
	 */
	public void valueChange(ValueChangeEvent event) {
		boolean enabled = (event.getProperty().getValue() != null)? true : false;
		this.enableButtons(enabled);
		
		return;
	}

	
	/**
	 * Return the list of actions of the table's context-menu.
	 */
	public Action[] getActions(Object target, Object sender) {
		return TABLE_ITEM_ACTIONS;
	}

	
	/**
	 * Handle click events on the table's context-menu.
	 */
	public void handleAction(Action action, Object sender, Object target) {
		
		this.groupsTable.select(target);
		
		if ((action == ACTION_VIEW) && (target != null)) {
			this.doViewGroupAction();
			
		}
		
		return;
	}
	
	
	/**
	 * Handle the closing event of the confirm dialog.
	 */
	public void onClose(ConfirmDialog dialog) {
		return;
	}	

	
	/**
	 * Handle the click events related to the action buttons.
	 */
	public void buttonClick(ClickEvent event) {
		Button source = event.getButton();
		
		if (source == this.buttonView) {
			this.doViewGroupAction();
		}
		
		return;
	}
	

	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ClientsGroupsListView.class.getName());
	}
}
