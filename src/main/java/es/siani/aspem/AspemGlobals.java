package es.siani.aspem;

public class AspemGlobals {
	
	// Configuration constants
	public static final String VAR_ENV_CONFIG_PATH = "ASPEM_CONFIG";
	
	public static final String PARAM_SYS_CONFIG_PATH = "aspemConfig";
	
	public static final String INTERNAL_CONFIG_PATH = "config-aspem.properties";
	
	public static Object[][] configPropsDescriptor = new Object[][] {
		{"aspem.name", "name", String.class, true},
		{"aspem.url", "aspemUrl", String.class, true},
		{"gridoperator.servicesPoint", "gridOperatorServicesPoint", String.class, true},
		{"xmpp.server.url", "xmppServerUrl", String.class, true},
		{"xmpp.server.port", "xmppServerPort", Integer.class, true},
		{"xmpp.user.login", "xmppUserLogin", String.class, true},
		{"xmpp.user.pass", "xmppUserPass", String.class, true},
		{"xmpp.user.resource", "xmppUserResource", String.class, true}		
	};
	
	// GUI constants
	public static final int TABLE_MAX_VISIBLE_ITEMS = 15;
	
	// Content
	public static final String ABOUT_RESOURCE = "about.html";
	public static final String CONTACT_RESOURCE = "contact.html";
}
