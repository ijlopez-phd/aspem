package es.siani.aspem.config;

public interface Configuration {
	
	public static String PROP_GRIDOP_SERVICES_POINT = "gridop.servicesPoint";
	
	public static String PROP_XMPP_URL = "xmpp.server.url";
	public static String PROP_XMPP_PORT = "xmpp.server.port";
	public static String PROP_XMPP_LOGIN = "xmpp.user.login";
	public static String PROP_XMPP_PASS = "xmpp.user.pass";
	public static String PROP_XMPP_RESOURCE = "xmpp.user.resource";	
	
	
	public String get(String propName);
	
}
