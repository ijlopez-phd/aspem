package es.siani.aspem.config;

import java.util.Hashtable;


public class ConfigurationImpl implements Configuration {
	
	/** Store for the configuration properties. */
	private Hashtable<String, String> props;
	
	
	/**
	 * Constructor.
	 */
	public ConfigurationImpl() {
		this.props = new Hashtable<String, String>();
		return;
	}
	
	
	/**
	 * Get a property as a String object.
	 */
	public String get(String propName) {
		
		if (!isValidPropName(propName)) {
			throw new IllegalArgumentException(
					"Failed to read property from the configuration file. The specified property is not supported.");			
		}
		
		return this.props.get(propName);
	}
	
	
	/**
	 * Set a configuration property.
	 */
	void set(String propName, String propValue) {
		this.props.put(propName, propValue);
		return;
	}
	

	
	/**
	 * Determines if the specified property name is valid.
	 */
	static boolean isValidPropName(String propName) {
		final String[] validFieldNames = ConfigurationLoader.validFieldNames;
		for (int n = 0; n < validFieldNames.length; n++) {
			if (propName.equals(validFieldNames[n])) {
				return true;
			}
		}
		
		return false;
	}	
}
