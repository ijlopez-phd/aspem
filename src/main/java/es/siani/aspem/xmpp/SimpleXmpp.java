package es.siani.aspem.xmpp;

import es.siani.aspem.AspemException;


public interface SimpleXmpp {
	
	/**
	 * Connect to the XMPP server.
	 */
	public abstract void connect() throws AspemException;
	

	/**
	 * Disconnect from the XMPP server.
	 */
	public abstract void disconnect();
	
	
	/**
	 * Check if it is connected to the server.
	 */
	public abstract boolean isConnected();

}
