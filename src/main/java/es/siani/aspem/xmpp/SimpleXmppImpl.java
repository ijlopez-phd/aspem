package es.siani.aspem.xmpp;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import es.siani.aspem.AspemException;
import es.siani.aspem.config.Configuration;
import static es.siani.aspem.config.Configuration.*;

public class SimpleXmppImpl implements SimpleXmpp {
	
	protected Connection connection;
	protected String serverUrl;
	protected int serverPort;
	protected String userLogin;
	protected String userPass;
	protected String userResource;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public SimpleXmppImpl(Configuration config) {
		this.serverUrl = config.get(PROP_XMPP_URL);
		this.serverPort = Integer.parseInt(config.get(PROP_XMPP_PORT));
		this.userLogin = config.get(PROP_XMPP_LOGIN);
		this.userPass = config.get(PROP_XMPP_PASS);
		this.userResource = config.get(PROP_XMPP_RESOURCE);
		return;
	}
	

	/**
	 * @see es.siani.aspem.xmpp.SimpleXmpp#connect()
	 */
	public void connect() throws AspemException {
		
		if ((this.connection != null) && (this.connection.isConnected())) {
			return;
		}
		
		ConnectionConfiguration xmppConfig = new ConnectionConfiguration(this.serverUrl, this.serverPort);
		this.connection = new XMPPConnection(xmppConfig);
		
		try {
			this.connection.connect();
			this.connection.login(this.userLogin, this.userPass, this.userResource);
		} catch (XMPPException ex) {
			getLogger().severe("Could not establish connection to the XMPP server.");
			throw new AspemException(ex);
		}
		
		return;
	}
	

	/**
	 * @see es.siani.aspem.xmpp.SimpleXmpp#disconnect()
	 */
	public void disconnect() {
		
		if ((this.connection == null) || (!this.connection.isConnected())) {
			return;
		}
		
		this.connection.disconnect();
		
		return;
	}
	
	
	/**
	 * @see es.siani.aspem.xmpp.SimpleXmpp#isConnected()
	 */
	public boolean isConnected() {
		if ((this.connection == null) || (!this.connection.isConnected())) {
			return false;
		}
		
		return true;
	}
	
	
	
	/**
	 * Logger.
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(SimpleXmppImpl.class.getName());
    }	

}
