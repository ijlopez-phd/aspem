package es.siani.aspem.model;

import java.io.Serializable;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlRootElement(name="client")
@XmlType(propOrder={"code", "alias", "description", "xmppAddress", "remoteXmppAddress", "clientsGroups"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Client extends BaseItem implements Serializable {

	@XmlTransient
	protected Integer globalId;
	
	@XmlElement
	protected String code;
	
	@XmlElement
	protected String alias;
	
	@XmlElement
	protected String description;
	
	@XmlElement
	protected String xmppAddress;
	
	@XmlElement
	protected String remoteXmppAddress;
	
	@XmlElementWrapper(name="clientsGroups") @XmlElement(name="clientsGroup")
	protected Set<ClientsGroup> clientsGroups;
	
	
	/**
	 * Ge the Global Id.
	 */
	public Integer getGlobalId() {
		return globalId;
	}
	
	
	/**
	 * Set the Global Id. 
	 */
	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}
	
	
	/**
	 * Get the TokenId property.
	 */
	public String getCode() {
		return this.code;
	}
	

	/**
	 * Set the TokenId property.
	 */
	public void setCode(String code) {
		this.code = code;
	}
	

	/**
	 * Get the Alias property. 
	 */
	public String getAlias() {
		return alias;
	}

	
	/**
	 * Set the Alias property. 
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	
	/**
	 * Get the Description property. 
	 */
	public String getDescription() {
		return description;
	}
	
	
	/**
	 * Set the Description property.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
	/**
	 * Get the XmppAddress. 
	 */
	public String getXmppAddress() {
		return xmppAddress;
	}

	
	/**
	 * Set the XmppAddress.
	 */
	public void setXmppAddress(String xmppAddress) {
		this.xmppAddress = xmppAddress;
	}
	
	
	/**
	 * Get the RemoteXmppAddress.
	 */
	public String getRemoteXmppAddress() {
		return remoteXmppAddress;
	}
	
	
	/**
	 * Set the RemoteXmppAddress.
	 */
	public void setRemoteXmppAddress(String remoteXmppAddress) {
		this.remoteXmppAddress = remoteXmppAddress;
	}
	
	
	/** 
	 * Get the list of Clients Groups.
	 */
	public Set<ClientsGroup> getClientsGroups() {
		return clientsGroups;
	}
	
	
	/**
	 * Set the list of Clients Groups.
	 */
	public void setClientsGroups(Set<ClientsGroup> clientsGroups) {
		this.clientsGroups = clientsGroups;
		return;
	}
}
