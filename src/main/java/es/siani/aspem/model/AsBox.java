package es.siani.aspem.model;

public class AsBox extends BaseItem {
	
	protected String name;
	protected int simulationId;
	
	
	/**
	 * Constructor.
	 */
	public AsBox() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AsBox(String name, int simulationId) {
		this.name = name;
		this.simulationId = simulationId;
		return;
	}
	
	
	/**
	 * Get the Name.
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Set the Name.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Get the Simulation Id.
	 */
	public int getSimulationId() {
		return simulationId;
	}
	
	
	/**
	 * Set the Simulation Id.
	 */
	public void setSimulationId(int simulationId) {
		this.simulationId = simulationId;
	}
}
