package es.siani.aspem.model;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Aspem implements Serializable {
	
	protected String code;
	protected String name;
	
	
	/**
	 * Constructor (default).
	 */
	public Aspem() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public Aspem(String code, String name) {
		this.code = code;
		this.name = name;
		return;
	}
	
	
	/**
	 * Get the Code.
	 */
	public String getCode() {
		return code;
	}
	
	
	/**
	 * Set the Code.
	 */
	public void setCode(String code) {
		this.code = code;
	}	
	
	
	/**
	 * Get the Name.
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Set the Name.
	 */
	public void setName(String name) {
		this.name = name;
	}
}
