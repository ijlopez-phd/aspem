package es.siani.aspem.model;

import javax.xml.bind.annotation.XmlElement;

public abstract class BaseItem {
	
	protected Integer id;
	
	
	/**
	 * Constructor.
	 */
	public BaseItem() {
		return;
	}
	
	
	/**
	 * Get the Id.
	 */
	@XmlElement
	public Integer getId() {
		return id;
	}
	
	
	/**
	 * Set the Id.
	 */	
	public void setId(Integer id) {
		this.id = id;
	}	

}
