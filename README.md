## ASPEM ##

( !! This application is still under development process)

### Description ###

ASPEM is part of the PhD thesis of **Ignacio Lopez-Rodriguez** at the [Institute for Intelligent Systems and Numeric Applications (SIANI)](http://www.siani.es), [University of Las Palmas de Gran Canaria (ULPGC)](http://www.ulpgc.es).

ASPEM stands for **Agency Services Provider for the Energy Management**. This concept is explained in greater depth in the following papers:

* _Agent-Based Services for Building Markets in Distributed Energy Environments_, on the _International Conference on Renewable Energies and Power Quality_ (ICREPQ), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2010.  

* _Challenges of using smart local devices for the management of the Smart Grid_, on the _IEEE International Conference on Smart Grid Communications_ (SmartGridComm), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2011.


ASPEM is a specific application of the more generic concept of **Agency Services**, which is fully described in the following papers:

* _Agency Services_, on the _International Conference on Agents and Artificial Intelligence_ (ICAART), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2010.  

* _Software Agents as Cloud Computing Services_, on Springer _Advances on Practical Applications of Agents and Multiagent Systems_ (PAAMS), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2011.
	
### Technologies ###

ASPEM has been developed with Java. The following Java technologies have been used:

* [Vaadin][] for creating web rich interfaces.

* [Jade][] for simulating a software agents' platform.

* [XMPP][] for the XMPP communications.

* [MyBatis][] as the persistence layer.

* [Google Guice][] for the dependency injection pattern.

* [Jersey][] for accessing RESTful services.

* [HSQLDB][] as the development environment database.

* [PostgreSQL][] as the production environment database.

* [Jetty][] as the Java Application Server.

* [Maven][] for the compilation and deploying processes.

* [Git][] as the version control system.

[Vaadin]: https://vaadin.com/home
[Jade]: http://jade.tilab.com/
[XMPP]: http://www.igniterealtime.org/projects/smack/
[MyBatis]: http://mybatis.org/core/index.html
[Google Guice]: http://code.google.com/p/google-guice/
[Jersey]: http://jersey.java.net/
[HSQLDB]: http://hsqldb.org/
[PostgreSQL]: http://www.postgresql.org/
[Jetty]: http://jetty.codehaus.org/jetty/
[Maven]: http://maven.apache.org/
[Git]: http://git-scm.com/
	
### License ###

_ASPEM_ is free software: you can redistribute it and/or modify it under the terms of the [**GNU Lesser General Public License**](http://www.gnu.org/licenses/lgpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

In short, the  _ASPEM_ application guarantees the four essential freedoms of Free Software, **and** it can be linked by proprietary software.

ASPEM is copyright (c) 2013 Ignacio Lopez-Rodriguez

